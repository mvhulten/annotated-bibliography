# Security Namespace

```
@Article{   sun2018
    ,title      = {Security Namespace: Making Linux Security Frameworks Available to Containers}
    ,author     = {Yuqiong Sun and David Safford and Mimi Zohar and Dimitrios Pendarakis and Zhongshu Gu and Trent Jaeger}
    ,booktitle  = {27th {USENIX} Security Symposium ({USENIX} Security 18)}
    ,year       = {2018}
    ,month      = aug
    ,isbn       = {978-1-939133-04-5}
    ,pages      = {1423--1439}
    ,pdf        = {https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-sun.pdf}
    ,keywords   = {computer science, containers, namespaces, security}
    ,comment    = {added:202106, read:202106}
}
```

## Annotation
*[Security Namespace](https://www.usenix.org/system/files/conference/usenixsecurity18/sec18-sun.pdf)*
discusses *containers* from a security point of view.
> *security namespaces* enable containers to obtain autonomous control
> over their security without compromising the security of other
> containers or the host system.

The *autonomous* part does typically not apply to namespaces, they
argue.
The problem is that in Linux security control is done *globally* and
*mandatory*.
The authors have introduced an abstraction (on Linux) that provides
autonomous security control for guest container systems.
Its overhead is less than 0.7%.

It looks to me that this paper is not about more security (to the host
operating system, or other containers) *per se* (like mitigating jail
break risk) but rather to give control to the container administrator,
akin to what you have with virtual machines (that include an operating
system kernel and hence full security control).

---

*Container* is not a primitive concept in the sense that they are
build from *namespaces*.
Of these namespaces these are often names separately or used as a
canonical example, but my understanding is that they are, or play an
important role to, namespaces:

+ PID namespaces: process started by chroot(2) becomes process zero,
like init(8) for the host system.  Linux does this with control groups
(cgroup).
+ Filesystem namespace: chroot(2) makes a new root in the virtual
filesystem.

The goal of containers (thus: namespaces) is to seal off a system
(container) from the host.
It is not trivial to do that in Unix (POSIX) in a secure way.
For instance a hardlink (with the wrong permissions) in the container to
a file in the host can cause a privilege escalation (for instance, root
in the container could modify the file).


# TITLE

```
@Article{   
    ,title      = {TITLE}
    ,year       = {}
    ,pages      = {--}
    ,pdf        = {FILE}
    ,keywords   = {computer science, }
    ,comment    = {added:}
}
```

## Annotation
*[TITLE](FILE)* discusses *SOMETHING* from a *CERTAIN* point of view.


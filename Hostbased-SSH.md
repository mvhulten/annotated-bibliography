# Hostbased SSH

```
@Article{   singer2014
    ,title      = {Hostbased SSH}
    ,subtitle   = {A Better Alternative}
    ,year       = {2014}
    ,pages      = {42--46}
    ,pdf        = {https://www.usenix.org/system/files/login/articles/09_singer.pdf}
    ,keywords   = {computer science, security}
    ,comment    = {added:202104, printed:202104}
}
```

## Annotation
[Hostbased
SSH](https://www.usenix.org/system/files/login/articles/09_singer.pdf)
is both an argumentative essay *for* and *how to setup* hostbased SSH
authentication.

The (idea for this) authentication method has evolved from trivial
(insecure) host-based authentication through `.rhosts` and the like,
to the more sophisticated (and secure) method described in this article.
The current host-based method uses public-key cryptography, just like is
used for (user based) *public-key authentication*.

An annoyance is that on a cluster (like Saga or Betzy) a host list needs
to be maintained.
Moreover they have to be in two different files, namely
+ `/etc/ssh/shosts.equiv` and
+ `/etc/ssh/ssh_known_hosts`.

Our `shosts.equiv` only lists the hosts, and user name restrictions are
not needed.
Strictly there is some kind of (relative) user check: I noticed that in
such configuration the user name on the client (that we control) has to
be the same as the one on the server (that we also control), so we have
good control here.
The `ssh_known_hosts` contains the same list of hosts, followed by the
cipher name and the host key.

Nonetheless, all in all it is a more secure and more controllable
approach than *public-key authentication*, because we don't have to
trust the users' private keys' security, or explain them what to put in
their `~/.ssh/` and what permissions the files need to be.


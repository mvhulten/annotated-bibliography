# Preventing Privilege Escalation

```
@InProceedings{provos2003
    ,title      = {Preventing Privilege Escalation}
    ,author     = {Provos, Niels and Friedl, Markus and Honeyman, Peter}
    ,booktitle  = {USENIX Security Symposium}
    ,year       = {2003}
    ,pages      = {231--242}
    ,url        = {https://www.usenix.org/legacy/events/sec03/tech/provos_et_al.html}
    ,pdf        = {https://www.usenix.org/legacy/events/sec03/tech/full_papers/provos_et_al/provos_et_al.pdf}
    ,keywords   = {computer science, security, OpenBSD}
    ,comment    = {added:202103, printed:202103}
}
```

## Annotation
In [Preventing Privilege
Escalation](https://www.usenix.org/legacy/events/sec03/tech/full_papers/provos_et_al/provos_et_al_html/index.html)
the author propose *privilege separation* as now implemented in OpenSSH.
It uses a parent–child model where the former acts as *monitor* and has
all privileges and the latter needs to ask for any privilege it needs.
The privileged parent (monitor) is modelled by a finite-state machine
(FSM).

FSM is not Turing-complete.
Models that are not Turing-complete are widely used for security
reasons.
Here it is in the form of a FSM (like traffic lights).
Bitcoin blockchain uses *sCrypt*, a language where complex
constructs like loops are not possible, ~~hence not Turing-complete.
I suppose that makes it safer than Ethereum.~~
Edit/correction, sCrypt has
[apparently](https://coingeek.com/bitcoin-script-is-turing-complete-scrypts-game-of-life-is-proof/)
been shown to be Turing-complete.
They base that on the fact that you can program Conway's Game of Life in
sCrypt, which would be proof indeed (as the Game of Life is shown to be
Turing-complete).
In any case, the relation between Turing-completeness and security is
hand-waving.
A better (albeit still hand-waving) argument is that sCrypt is more
limited and in practice there have been lots of hacks with smart
contracts on Ethereum but nothing similar on the Bitcoin blockchain.

The authors use Inter-Process Communication (IPC) for communication
between the *monitor* (parent) and the child.
Unix sockets are used for this.

The whole authentication process in OpenSSH starts with *authenticated
key exchange* where the client requires crypotographic proof of the
server identity.
This is achieved by checking the fingerprint in
`~/.ssh/authorized_keys`.
Then the username is checked by the server through an *informational
request* to the server (so the monitor will handle this).
Whenever possible, informational requests are used; *capability* or
*change of identity requests* are only allowed when needed.

This can be seen as part of the *least privilege philosophy*
that the authors try to convey (citing papers from the 1970s).
OpenBSD philosophy drips from the article:
> Privilege separation requires clean and well abstracted subsystem
> interfaces [...]
> As a result, the source code is better organised, more easily
> understood and audited, and less complex.

Password or public key authentication follows.
I suppose this will follow by Diffie-Hellman key exchange follows to
establish a secure symmetric channel (goes beyond the monitor model, so
it is not described in the paper).

I think all this is implemented in *BSD Auth*.
Other operating systems use *Pluggable Authentication Modules* (PAM).
I wonder how well PAM follows the philosophies conveyed in this paper...
NetBSD has had
[discussions](http://mail-index.netbsd.org/current-users/2011/08/19/msg017423.html)
about it.
I cannot judge (did not look at either of the codes).

Most interesting and novel in this paper (*of 2003*) is the way how the
privilege level is increase for the client process.
Its state is copied to the parent who spawns a process with the right
UID and so on.
It seems a bit hacky, but apparently on most Unix systems it is not
possible to change the user identity (see footnote on p. 234 and
thereabove), so a new process must be spawned.

Other attacks are possible, of course:
- DoS attacks—monitor should limit number of requests;
- a ptrace(2) could be used to get information from other SSH sessions;
  this is mitigated through changing UID; reminds me about ssh-agent(1)
  being set an unused group as I noted in my «ssh auth proposal» for
  Sigma2 where users may use authentication information of a private key
  s/he cannot read;
- the introduced interface may be attacked.

Also a security issue in Kerberos is mentioned; OpenBSD does not support
Kerberos [anymore](https://undeadly.org/cgi?action=article;sid=20140425065910).
Maybe it is good that Kerberos cannot easily be «plugged in» into the
authentication system (which is BSD Auth, not PAM).


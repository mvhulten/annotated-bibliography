# Setuid Demystified

```
@Article{   chen2018
    ,title      = {Setuid Demystified}
    ,year       = {2018}
    ,pdf        = {https://www.cs.berkeley.edu/~daw/papers/setuid-usenix02.pdf}
    ,keywords   = {computer science, security}
    ,comment    = {added:202106}
}
```

## Annotation
[Setuid Demystified](https://www.cs.berkeley.edu/~daw/papers/setuid-usenix02.pdf) discusses \emph{} ...

